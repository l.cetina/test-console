﻿using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class Test
    {
        private Subject<UpdateDataCommand> posted = new Subject<UpdateDataCommand>();
        public Test()
        {
            PostInitialize();
        }

        private void PostInitialize()
        {
            posted
                .GroupBy(x => x.StationId)
                .Select(gxs =>
                    gxs
                        .Select(x =>
                            Observable
                                .Timer(TimeSpan.FromSeconds(10))
                                .Select(_ => x))
                        .Switch())
                .Merge()
                .Subscribe(stationId =>
                {
                    Console.WriteLine(stationId.StationId);
                });
        }

        public async Task Post(UpdateDataCommand cmd)
        {
            await Console.Out.WriteLineAsync($"{DateTime.Now.ToLongTimeString()}  Posting...");
            posted.OnNext(cmd);
        }
    }


    public class UpdateDataCommand
    {
        public int StationId { get; set; }
    }
}
