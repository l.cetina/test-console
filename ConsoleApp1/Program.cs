﻿using System;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {

        async static Task Main(string[] args)
        {

            await Task.Run(() =>
            {
                var test = new Test();
                return test.Post(new UpdateDataCommand() {StationId = 1});
            });
            await Task.Delay(TimeSpan.FromSeconds(6));
            await Task.Run(() =>
            {
                var test = new Test();
                return test.Post(new UpdateDataCommand() {StationId = 1});
            });

            await Task.Delay(TimeSpan.FromSeconds(20));
            await Console.Out.WriteLineAsync("End");
        }

       
    }

   
}
